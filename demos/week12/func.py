def fib(n):
    """Returns the nth Fibonacci number."""
    a = 0
    b = 1
    for i in range(n):
        tmp = a
        a = a + b
        b = tmp
    return a

print("fib(5) =", fib(5))
