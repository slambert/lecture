# Booleans: like in C++ but with capital T and F
True, False

# Integers
x = 1

# Floats: like in C++ with a dot (almost all platforms map Python floats
# to "double precision")
x = 1.
x = 1.e0
x = 1.E0

# Complex numbers
x = 1 + 1j
x = 1 + 1J

# Strings: with single or double quotes
x = 'string'
x = "string"
x = "Hello! I'm a string!"
x = 'Hi! I\'m "another" string!'
