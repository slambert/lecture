import point # load the point module

# some more stuff for your application... functions, classes, ...

if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1: # check number of command line arguments
        P1 = point.Point(1., int(sys.argv[1])) # get P1's y coordinate from
                                               # command line arguments
    else:
        P1 = point.Point(1., 2.)
    P2 = point.Point(0., 7.)
    P3 = P1 + P2
    print(P3)
