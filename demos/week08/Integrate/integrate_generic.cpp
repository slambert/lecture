#include <iostream>
#include <cmath>

// trapezoidal rule
template <typename T,typename F>
T integrate(F f, T a, T b, unsigned int N) {
  T dx = (b - a )/N;
  T xi = a;
  T I = 0.5*f(xi);
  for (unsigned int i = 1; i < N; ++i) {
    xi += dx;
    I += f(xi);
  }
  I += 0.5*f(b);
  return I*dx;
}

struct func {
  double operator()(double x) { return x*std::sin(x); }
};

int main() {
  std::cout <<"I[x*sin(x)] = " << integrate(func(), 0., 1., 100) << '\n';

  return 0;
}
