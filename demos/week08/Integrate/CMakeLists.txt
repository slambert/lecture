cmake_minimum_required(VERSION 3.1)

project(PT1_week07_Integrate)

# use C++11 (globally)
set(CMAKE_CXX_STANDARD 11)          # set standard to C++11
set(CMAKE_CXX_STANDARD_REQUIRED ON) # requires C++11
set(CMAKE_CXX_EXTENSIONS OFF)       # disables extensions such as -std=g++XX

# setting warning compiler flags
add_compile_options(-Wall -Wextra -Wpedantic)

add_executable(integrate_procedural integrate_procedural.cpp)
add_executable(integrate_generic integrate_generic.cpp)
add_executable(integrate_oo integrate_oo.cpp)
