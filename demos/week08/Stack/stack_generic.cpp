#include <iostream>
#include <stdexcept>

namespace Stack {

  template <typename T>
  class stack {
    public:
      stack(int l) : s(new T[l]), p(s), n(l) {} // ctor
      ~stack() { delete[] s; }                  // dtor

      void push(T v) {
        if ( p == s + n - 1 ) {
          throw std::runtime_error("stack overflow");
        }
        *p++ = v;
      }

      T pop() {
        if ( p == s ) {
          throw std::runtime_error("stack underflow");
        }
        return *--p;
      }
    private:
      T* s;      // pointer to allocated stack memory
      T* p;      // stack pointer
      int n;     // stack size
  };

}

int main() {
  try {
    Stack::stack<double> s(100);
    s.push(10.);
    std::cout << s.pop() << '\n';

    Stack::stack<int> si(100);
    si.push(10);
    std::cout << si.pop() << '\n';

  }
  catch (std::exception& e) {
    std::cerr << "Exception occurred: " << e.what() << '\n';
    return 1; // return error, i.e. non-zero value!
  }

  return 0; // return 0, i.e. 0 for zero problems!
}
