#include <iostream>

class Polygon {
  public:
    void set_values(double a, double b) {
      width_  = a;
      height_ = b;
    }
    virtual double area() {
      return 0.;
    }
  protected:
    double width_, height_;
};

class Rectangle: public Polygon {
  public:
    double area() {
      return width_*height_;
    }
};

class Triangle: public Polygon {
  public:
    double area() {
      return 0.5*width_*height_;
    }
};

void set_values(Polygon& poly, double a, double b) {
  poly.set_values(a, b);
}
  
int main() {
  Polygon* poly;
  Rectangle rec;
  Triangle  tri;

  poly = &rec;
  poly->set_values(4., 5.);
  std::cout << "Rectangle area: " << poly->area() << '\n';

  poly = &tri;
  poly->set_values(4., 5.);
  std::cout << "Triangle  area: " << poly->area() << '\n';

  set_values(rec, 4., 5.);
  std::cout << "Rectangle area: " << poly->area() << '\n';

  set_values(tri, 4., 5.);
  std::cout << "Triangle  area: " << poly->area() << '\n';

  return 0;
}
