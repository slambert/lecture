cmake_minimum_required(VERSION 3.1)

project(PT1_week10)

# use C++11 (globally)
set(CMAKE_CXX_STANDARD 11)          # set standard to C++11
set(CMAKE_CXX_STANDARD_REQUIRED ON) # requires C++11
set(CMAKE_CXX_EXTENSIONS OFF)       # disables extensions such as -std=g++XX

# needs BLAS!
find_package(BLAS)
if ( BLAS_FOUND )
  add_executable(blas_dot blas_dot.cpp)
  target_link_libraries(blas_dot ${BLAS_LIBRARIES})
  add_executable(blas_levels blas_levels.cpp timer.cpp)
  target_link_libraries(blas_levels ${BLAS_LIBRARIES})
else()
  message("BLAS not found")
endif()

# needs LAPACK!
find_package(LAPACK)
if ( LAPACK_FOUND )
  add_executable(lapack_dgesv lapack_dgesv.cpp)
  target_link_libraries(lapack_dgesv ${LAPACK_LIBRARIES})
else()
  message("LAPACK not found")
endif()

# needs GSL!
find_package(GSL)
if ( GSL_FOUND )
  add_executable(gsl_demo gsl_demo.cpp)
  target_include_directories(gsl_demo PUBLIC ${GSL_INCLUDE_DIRS})
  target_link_libraries(gsl_demo ${GSL_LIBRARIES})
else()
  message("GSL not found")
endif()
