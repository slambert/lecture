#include <algorithm> // for std::swap
#include <cassert>

// This class encapsulates the "+" operation.
struct plus {
  public:
   static inline double apply(double a, double b) {
     return a + b;
   }
};

// This class encapsulates the "-" operation.
struct minus {
  public:
    static inline double apply(double a, double b) {
      return a - b;
    }
};


// forward-declare vectorop class as it is needed in lazyvector class
// now also templated on the Operation!
template <typename T,typename Op>
class vectorop;

// lazyvector
template <typename T>
class lazyvector {
  public:
    typedef T value_type;
    typedef T& reference;
    typedef unsigned int size_type;
    // ctor
    explicit lazyvector(size_type s=0) : p_(new value_type[s]), sz_(s) {}
    // copy ctor
    lazyvector(const lazyvector& v) : p_(new value_type[v.size()])
                                    , sz_(v.size()) {
      for (int i=0; i<size(); ++i) {
        p_[i] = v.p_[i];
      }
    }
    // dtor
    ~lazyvector() { delete[] p_; }
    // swap
    void swap(lazyvector& v) {
      std::swap(p_,v.p_);
      std::swap(sz_,v.sz_);
    }
    // copy assignment
    lazyvector& operator=(lazyvector v) {
      swap(v);
      return *this;
    }
    // assignment from a vectorop object
    template <typename Op>
    const lazyvector& operator=(const vectorop<T,Op>& v) {
      for (int i=0; i<size(); ++i) {
        p_[i] = v[i];
      }
      return *this;
    }
    // size
    size_type size() const { return sz_; }
    // subscript operator
    value_type operator[](size_type i) const { return p_[i]; }
    reference operator[](size_type i) { return p_[i]; }

  private:
    value_type* p_;
    size_type sz_;
};

// vectorop class
template <typename T,typename Op>
class vectorop {
  public:
    typedef unsigned int size_type;
    typedef T value_type;
    // ctor
    vectorop(const lazyvector<T>& x, const lazyvector<T>& y) : left_(x)
                                                             , right_(y) {}
    // subscript operator
    value_type operator[](size_type i) const {
      return Op::apply(left_[i], right_[i]);
    }

  private:
    const lazyvector<T>& left_;
    const lazyvector<T>& right_;
};


// binary "+" operator (non-member / free function)
template <typename T>
inline vectorop<T,plus> operator+(const lazyvector<T>& x,
                                  const lazyvector<T>& y) {
  return vectorop<T,plus>(x, y);
}

// binary "-" operator (non-member / free function)
template <typename T>
inline vectorop<T,minus> operator-(const lazyvector<T>& x,
                                   const lazyvector<T>& y) {
  return vectorop<T,minus>(x ,y);
}
