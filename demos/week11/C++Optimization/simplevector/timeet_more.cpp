#include <boost/core/demangle.hpp>
#include <iostream>
#include <typeinfo>
#include "etvector_more.hpp"

struct Dummy {

};

int main() {
  const int N = 10000000;
  etvector<double> a(N), b(N), c(N), d(N);
/*
  for (int i=0; i<100; ++i) {
    a = b + b*b + (c*d + b);
    a = (b + c) + (d + d) + Dummy();
  }
*/
  std::cout << "type: " << boost::core::demangle(typeid(b + c + (d + d)).name()) << '\n';
  std::cout << "type: " << boost::core::demangle(typeid( 1. + 2).name()) << '\n';
  std::cout << "type: " << boost::core::demangle(typeid( Dummy() + Dummy()).name()) << '\n';
  std::cout << a[0] << std::endl;
}
