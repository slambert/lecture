#include <iostream>
#include <iomanip>
#include <sstream>

int main() {

  for (int i = 0; i < 5; ++i) {
    std::ostringstream ss{};
    ss << std::setw(5) << std::setfill('0') << i;
    std::string s2(ss.str());
    std::cout << ss.str() << '\n';
  }
}
