/*
 * Programming Techniques for Scientific Simulations I
 * HS 2022
 * Exercise 10
 */

#include "matrix_multiplication.hpp"
#include <Eigen/Core>

void mm_eigen(matrix_t const & A, matrix_t const & B, matrix_t & C, std::size_t N) noexcept {
	auto const Ae = Eigen::Map<Eigen::MatrixXd const>(A.data(), N, N);
	auto const Be = Eigen::Map<Eigen::MatrixXd const>(B.data(), N, N);
	auto       Ce = Eigen::Map<Eigen::MatrixXd>(      C.data(), N, N);
	Ce = Ae * Be;
}
