#include <stdexcept>

#include "simpson.hpp"

Function::result_type integrate(const Function::argument_type a,
                                const Function::argument_type b,
                                const unsigned int bins,
                                const Function& func) {
    if (bins == 0) {
        throw std::logic_error("integrate(..) : Number of bins has to be positive.");
    }

    using argument_t = Function::argument_type;
    using result_t = Function::result_type;

    const argument_t dr = (b - a) / static_cast<argument_t>(2*bins);
    result_t I2(0), I4(0);
    argument_t pos = a;

    for (unsigned int i = 0; i < bins; ++i) {
        pos += dr;
        I4 += func(pos);
        pos += dr;
        I2 += func(pos);
    }

    return (func(a) + 2.0 * I2 + 4.0 * I4 - func(b)) * (dr / 3.0);
}
