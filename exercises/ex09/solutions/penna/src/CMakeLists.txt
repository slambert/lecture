include_directories(${PROJECT_SOURCE_DIR}/src)

add_library(penna STATIC genome.cpp animal.cpp population.cpp fish_population.cpp)

add_library(penna_vector STATIC genome.cpp animal.cpp population.cpp fish_population.cpp)
set_target_properties(penna_vector PROPERTIES COMPILE_FLAGS "-DPENNA_VECTOR")
