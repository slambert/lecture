/**
 * Implementation of the Penna population class.
 * Programming Techniques for Scientific Simulations, ETH Zürich
 */

#include "population.hpp"
#include <vector>
#include <algorithm>
#include <functional>
#include <cstdlib>
#include <iostream>

namespace Penna {

Population::Population( const size_t & nmax, const size_t & n0 )
    : population_( n0 ), nmax_( nmax ) {
}

// We could default the dtor in the hpp, but doing it here keeps the flexibility
// to change it.
Population::~Population() = default;

void Population::simulate( size_t time ) {
    while( time-- ) step();
}

class DeathPredicate {
public:
    DeathPredicate( const double & probability ) : probability_(probability) {};

    bool operator()( Animal const& a ) const {
        return probability_ >= 1. or a.is_dead() or (rand() / double(RAND_MAX)) < probability_;
    };

private:
    const double probability_;
};

void Population::step() {
    // Age all fishes
    // Use C++11 features where they are more elegant than STL algorithms.
    for(auto& animal : population_)
        animal.grow();

    // Remove dead ones
    population_.remove_if( DeathPredicate( size() / double( nmax_ ) ) );

    // Generate offsprings
    // The range-for solution did not work for std::vector.
    // See https://en.cppreference.com/w/cpp/container#Iterator_invalidation
    std::vector<Animal> kids;
    for(auto it=population_.begin(); it != population_.end(); ++it)
        if(it->is_pregnant())
            kids.push_back(it->give_birth());
    std::move(kids.begin(), kids.end(), std::back_inserter(population_));
    // NOTE: we are using the std::move algorithm.
    //       This is not the more common std::move from the utility header.
}

std::size_t Population::size() const {
    return population_.size();
}

Population::const_iterator Population::begin() const {
    return population_.begin();
}

Population::const_iterator Population::end() const {
    return population_.end();
}

} // end namespace Penna
